Feature: Delete an existing hotel reservation
  As a user, I want to be delete a reservation.

  Scenario: Delete an existing reservation
    Given a user is on the hotel booking page
    And the following reservation exists
      | firstname | surname | price | deposit | checkin    | checkout   |
      | Gee       | Bloggs  | 79    | false   | 2017-11-29 | 2017-12-09 |
    When the reservation is deleted
    Then the reservation is not on the list