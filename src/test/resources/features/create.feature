Feature: Place a hotel reservation
  As a user, I want to be able to place a reservation for given time period and optionally provide a deposit.

  Scenario:
    Given a user is on the hotel booking page
    And the following reservation does not exist
      | firstname | surname | price | deposit | checkin    | checkout   |
      | Gee       | Bloggs  | 79    | false   | 2017-11-29 | 2017-12-09 |
    When the required information is entered on the page
    And the save button is clicked
    Then the reservation appears on the list