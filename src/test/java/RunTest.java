import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = "target/test-classes",
        glue = "step_definitions",
        plugin = {"pretty", "html:target/cucumber-report"})
public class RunTest extends AbstractTestNGCucumberTests{
}
