package model;

public class BookingDetails {
    private String firstname;
    private String surname;
    private String price;
    private String deposit;
    private String checkin;
    private String checkout;

    public BookingDetails(String firstname, String surname, String price, String deposit, String checkin, String checkout) {
        this.firstname = firstname;
        this.surname = surname;
        this.price = price;
        this.deposit = deposit;
        this.checkin = checkin;
        this.checkout = checkout;
    }

    public String firstname() {
        return firstname;
    }

    public String surname() {
        return surname;
    }

    public String price() {
        return price;
    }

    public String deposit() {
        return deposit;
    }

    public String checkin() {
        return checkin;
    }

    public String checkout() {
        return checkout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookingDetails details = (BookingDetails) o;

        if (firstname != null ? !firstname.equals(details.firstname) : details.firstname != null) return false;
        if (surname != null ? !surname.equals(details.surname) : details.surname != null) return false;
        if (price != null ? !price.equals(details.price) : details.price != null) return false;
        if (deposit != null ? !deposit.equals(details.deposit) : details.deposit != null) return false;
        if (checkin != null ? !checkin.equals(details.checkin) : details.checkin != null) return false;
        return checkout != null ? checkout.equals(details.checkout) : details.checkout == null;
    }

    @Override
    public int hashCode() {
        int result = firstname != null ? firstname.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (deposit != null ? deposit.hashCode() : 0);
        result = 31 * result + (checkin != null ? checkin.hashCode() : 0);
        result = 31 * result + (checkout != null ? checkout.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BookingDetails{" +
                "firstname='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                ", price='" + price + '\'' +
                ", deposit='" + deposit + '\'' +
                ", checkin='" + checkin + '\'' +
                ", checkout='" + checkout + '\'' +
                '}';
    }
}
