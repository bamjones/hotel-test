package helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.logging.Level;

public abstract class WebDriverHelper extends EventFiringWebDriver {
    private static final Thread CLOSE_THREAD = new Thread() {
        public void run() {
            DRIVER.quit();
        }
    };
    private static WebDriver DRIVER = null;

    static {
        String DRIVER_ROOT_DIR = PropertiesHelper.getProperty("driver.root.dir");

        System.setProperty("webdriver.chrome.driver", DRIVER_ROOT_DIR);

        startChromeDriver();

        Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
    }

    public WebDriverHelper() {
        super(DRIVER);
    }

    private static void startChromeDriver() {
        DesiredCapabilities capabilities = getChromeDesiredCapabilities();

        DRIVER = new ChromeDriver(ChromeDriverService.createDefaultService(), capabilities);
    }

    private static DesiredCapabilities getChromeDesiredCapabilities() {
        LoggingPreferences logs = new LoggingPreferences();
        logs.enable(LogType.DRIVER, Level.OFF);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--disable-web-security");
        chromeOptions.addArguments("--test-type");

        capabilities.setCapability("chrome.verbose", false);

        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        return capabilities;
    }

    public static WebDriver driver() {
        return DRIVER;
    }
}
