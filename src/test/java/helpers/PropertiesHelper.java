package helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.System.out;

class PropertiesHelper {
    private static final Logger LOG = LoggerFactory.getLogger(PropertiesHelper.class);

    private static Properties properties;

    static {
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("mac")) {
            loadEnvironmentProperties("/config/mac/config.properties");
        } else if (os.contains("win")) {
            loadEnvironmentProperties("/config/windows/config.properties");
        } else {
            throw new RuntimeException("OS not supported");
        }
    }

    static String getProperty(String key) {
        if (key == null || key.isEmpty()) {
            return "";
        } else {
            return properties.getProperty(key);
        }
    }

    private static void loadEnvironmentProperties(String envPropsFileLocation) {
        properties = new Properties();

        try (InputStream inputStream = PropertiesHelper.class.getResourceAsStream(envPropsFileLocation)) {
            properties.load(inputStream);
            properties.list(out);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }
}
