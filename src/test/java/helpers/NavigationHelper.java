package helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class NavigationHelper {
    private static final Logger LOG = LoggerFactory.getLogger(NavigationHelper.class);

    private static URL basePath;

    static {
        try {
            basePath = new URL(PropertiesHelper.getProperty("page.url"));
        } catch (MalformedURLException e) {
            LOG.error(e.getMessage());
        }
    }

    public static void goToBookingPage() {
        WebDriverHelper.driver().navigate().to(basePath);
    }
}
