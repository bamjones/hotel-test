package page_objects;

import helpers.WebDriverHelper;
import model.BookingDetails;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Optional;

public class BookingPage {

    private WebDriver driver;

    @FindBy(id = "firstname")
    private WebElement firstNameField;

    @FindBy(id = "lastname")
    private WebElement surnameField;

    @FindBy(id = "totalprice")
    private WebElement price;

    @FindBy(id = "depositpaid")
    private WebElement deposit;

    @FindBy(id = "checkin")
    private WebElement checkin;

    @FindBy(id = "checkout")
    private WebElement checkout;

    @FindBy(css = "input[onclick^=createBooking]")
    private WebElement saveButton;

    @FindBy(css = ".row")
    private List<WebElement> rows;

    public BookingPage() {
        this.driver = WebDriverHelper.driver();
        PageFactory.initElements(driver, this);
    }

    public void setBooking(BookingDetails details) {
        firstNameField.sendKeys(details.firstname());
        surnameField.sendKeys(details.surname());
        price.sendKeys(details.price());
        new Select(deposit).selectByVisibleText(details.deposit());
        checkin.sendKeys(details.checkin());
        checkout.sendKeys(details.checkout());
    }

    public void saveBooking() throws InterruptedException {
        saveButton.click();
    }

    public boolean find(BookingDetails bookingDetails) {
        return getRowFromPageFor(bookingDetails).isPresent();
    }

    public Optional<String> getId(BookingDetails currentBooking) {
        return getRowFromPageFor(currentBooking).map(row -> row.getAttribute("id"));
    }

    private Optional<WebElement> getRowFromPageFor(BookingDetails bookingDetails) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 5);
            return Optional.ofNullable(wait.until((ExpectedCondition<WebElement>) driver1 -> {
                List<WebElement> dataList = dataRows();
                for (WebElement row : dataList) {
                    BookingDetails bookingDetailsFromPage = getBookingDetailsFromPage(row);
                    if (bookingDetailsFromPage.equals(bookingDetails)) {
                        return row;
                    }
                }
                return null;
            }));
        } catch (TimeoutException e) {
            // ignore and return false
        }
        return Optional.empty();
    }

    public void deleteBooking(String id) {
        By deleteButton = By.cssSelector("input[onclick=deleteBooking\\(" + id + "\\)]");

        driver.findElement(deleteButton).click();
    }

    private List<WebElement> dataRows() {
        return rows.subList(1, rows.size() - 1);
    }

    private BookingDetails getBookingDetailsFromPage(WebElement row) {
        List<WebElement> columns = row.findElements(By.tagName("div"));

        String firstname = columns.get(0).getText();
        String surname = columns.get(1).getText();
        String price = columns.get(2).getText();
        String deposit = columns.get(3).getText();
        String checkin = columns.get(4).getText();
        String checkout = columns.get(5).getText();

        return new BookingDetails(firstname, surname, price, deposit, checkin, checkout);
    }
}

