package step_definitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.NavigationHelper;
import model.BookingDetails;
import org.openqa.selenium.StaleElementReferenceException;
import page_objects.BookingPage;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class BookingSteps {
    private BookingPage bookingPage = new BookingPage();
    private BookingDetails currentBooking;

    @Given("^a user is on the hotel booking page$")
    public void aUserIsOnTheHotelBookingPage() throws Throwable {
        NavigationHelper.goToBookingPage();
    }

    @And("^the following reservation does not exist$")
    public void reservationDoesNotExist(List<BookingDetails> bookingDetails) throws Throwable {
        setCurrentBooking(bookingDetails);

        assertThat(bookingPage.find(currentBooking))
                .describedAs("Booking %s already exists", bookingDetails)
                .isFalse();

    }

    @When("^the required information is entered on the page")
    public void theRequiredInformationIsProvided() throws Throwable {
        bookingPage.setBooking(currentBooking);
    }

    @And("^the save button is clicked$")
    public void theSaveButtonIsClicked() throws Throwable {
        bookingPage.saveBooking();
    }

    @Then("^the reservation appears on the list$")
    public void theReservationAppearsOnTheList() throws Throwable {
        assertThat(bookingPage.find(currentBooking))
                .describedAs("Booking %s does not exist", currentBooking)
                .isTrue();
    }

    @And("^the following reservation exists$")
    public void reservationExists(List<BookingDetails> bookingDetails) throws Throwable {
        setCurrentBooking(bookingDetails);

        if (!bookingPage.find(currentBooking)) {
            theRequiredInformationIsProvided();
            theSaveButtonIsClicked();
            theReservationAppearsOnTheList();
        }
    }

    @When("^the reservation is deleted$")
    public void theReservationIsDeleted() throws Throwable {
        Optional<String> id = bookingPage.getId(currentBooking);

        bookingPage.deleteBooking(id.orElseThrow(() ->
                new NoSuchElementException("Did not get an id for booking: " + currentBooking.toString())));
    }

    private void setCurrentBooking(List<BookingDetails> bookingDetails) {
        currentBooking = currentBooking == null ? bookingDetails.get(0) : currentBooking;
    }

    @Then("^the reservation is not on the list$")
    public void theReservationIsNotOnTheList() throws Throwable {
        boolean found;
        try {
            found = bookingPage.find(currentBooking);
        } catch (StaleElementReferenceException e) {
            // ignore since it has probably been deleted
            found = false;
        }
        assertThat(found).describedAs("Found booking: " + currentBooking.toString()).isFalse();
    }
}
